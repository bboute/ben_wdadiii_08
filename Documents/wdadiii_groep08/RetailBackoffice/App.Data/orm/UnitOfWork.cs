﻿using App.Data.orm.repositories;
using App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Data.orm
{
    public class UnitOfWork
    {
        #region VARIABLES
        private RetailContext _context = new RetailContext();
        private RetailRepository<Article> _articleRepository;
        private RetailRepository<User> _userRepository;
        private RetailRepository<Person> _personRepository;
        private RetailRepository<Member> _memberRepository;
        private RetailRepository<Category> _categoryRepository;
        private RetailRepository<EntityComment> _entityCommentRepository;
        private RetailRepository<Message> _messageRepository;
        private RetailRepository<Role> _roleRepository;
        #endregion

        #region Repository Properties
        public RetailRepository<Article> ArticleRepository
        {
            get
            {
                if (_articleRepository == null)
                    _articleRepository = new RetailRepository<Article>(_context);
                return _articleRepository;
            }
        }
        public RetailRepository<User> UserRepository
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new RetailRepository<User>(_context);
                return _userRepository;
            }
        }
        public RetailRepository<Person> PersonRepository
        {
            get
            {
                if (_personRepository == null)
                    _personRepository = new RetailRepository<Person>(_context);
                return _personRepository;
            }
        }
        public RetailRepository<Member> MemberRepository
        {
            get
            {
                if (_memberRepository == null)
                    _memberRepository = new RetailRepository<Member>(_context);
                return _memberRepository;
            }
        }
        
        public RetailRepository<Category> CategoryRepository
        {
            get
            {
                if (_categoryRepository == null)
                    _categoryRepository = new RetailRepository<Category>(_context);
                return _categoryRepository;
            }
        }
        public RetailRepository<EntityComment> EntityCommentRepository
        {
            get
            {
                if (_entityCommentRepository == null)
                    _entityCommentRepository = new RetailRepository<EntityComment>(_context);
                return _entityCommentRepository;
            }
        }
        public RetailRepository<Message> MessageRepository
        {
            get
            {
                if (_messageRepository == null)
                    _messageRepository = new RetailRepository<Message>(_context);
                return _messageRepository;
            }
        }
        public RetailRepository<Role> RoleRepository
        {
            get
            {
                if (_roleRepository == null)
                    _roleRepository = new RetailRepository<Role>(_context);
                return _roleRepository;
            }
        }
        #endregion

        #region METHODS
        public void Save()
        {
            _context.SaveChanges();
        }
        #endregion
    }
}
