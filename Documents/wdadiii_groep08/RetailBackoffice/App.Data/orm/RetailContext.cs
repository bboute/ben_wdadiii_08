﻿using App.Data.orm.mappings;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Data.orm
{
    public class RetailContext:DbContext
    {
        public RetailContext() : base("retailCS") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //REMOVE STANDARD MAPPING IN ENTITY FRAMEWORK
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //REGISTER MAPPERS
            modelBuilder.Configurations.Add(new UserMapping());
            modelBuilder.Configurations.Add(new PersonMapping());
            modelBuilder.Configurations.Add(new MemberMapping());

            modelBuilder.Configurations.Add(new EntityMapping()); 
            modelBuilder.Configurations.Add(new ArticleMapping());
            modelBuilder.Configurations.Add(new CategoryMapping());
            modelBuilder.Configurations.Add(new EntityCommentMapping());
            modelBuilder.Configurations.Add(new MessageMapping());
            modelBuilder.Configurations.Add(new RoleMapping());
        }
    }
}
