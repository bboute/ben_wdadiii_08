﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using App.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Data.orm.mappings
{
    internal class EntityMapping : EntityTypeConfiguration<Entity>
    {
        public EntityMapping()
            : base()
        {
            //tabel aanspreken
            this.ToTable("entities", "retail");
            //velden aanspreken
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("entity_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Title).HasColumnName("entity_title").IsRequired().HasMaxLength(255);
            this.Property(t => t.Description).HasColumnName("entity_description").IsRequired();
            this.Property(t => t.Price).HasColumnName("entity_price").IsRequired();
            this.Property(t => t.CreateDate).HasColumnName("entity_created").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            this.Property(t => t.ModifiedDate).HasColumnName("entity_modified").IsOptional();
            this.Property(t => t.DeletedDate).HasColumnName("entity_deleted").IsOptional();
            //foreign key
            this.Property(t => t.UserId).HasColumnName("user_id").IsRequired();
            
            //
            this.HasRequired(t => t.User).WithMany(p => p.Entities).HasForeignKey(f => f.UserId).WillCascadeOnDelete();

            //MANY_TO_MANY MAPPINGS
            this.HasMany(t => t.Categories)
                .WithMany(t => t.Entities)
                .Map(mc =>
                    {
                        mc.ToTable("entities_has_categories");
                        mc.MapLeftKey("entity_id");
                        mc.MapRightKey("category_id");
                    });
            

        }
    }
}
