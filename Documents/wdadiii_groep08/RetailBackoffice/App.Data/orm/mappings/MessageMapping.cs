﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using App.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Data.orm.mappings
{
    internal class MessageMapping : EntityTypeConfiguration<Message>
    {
        public MessageMapping()
            : base()
        {
            this.ToTable("messages", "retail");

            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("message_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Body).HasColumnName("message_body").IsRequired();
            this.Property(t => t.CreateDate).HasColumnName("message_created").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            this.Property(t => t.DeletedDate).HasColumnName("message_deleted").IsOptional();

            this.Property(t => t.UserId).HasColumnName("user_id").IsRequired();
            this.Property(t => t.ReceiverId).HasColumnName("receiver_id").IsRequired();

            //FOREIGN KEY MAPPINGS
            this.HasRequired(t => t.User).WithMany(p => p.SentMessages).HasForeignKey(f => f.UserId).WillCascadeOnDelete();
            this.HasRequired(t => t.Receiver).WithMany(p => p.ReceivedMessages).HasForeignKey(f => f.ReceiverId).WillCascadeOnDelete();
        }
    }
}
