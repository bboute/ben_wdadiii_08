﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using App.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Data.orm.mappings
{
    internal class ArticleMapping : EntityTypeConfiguration<Article>
    {
        public ArticleMapping()
            : base()
        {
            this.ToTable("articles", "retail");
            this.Property(t => t.Body).HasColumnName("article_body").IsRequired();
        }
    }
}
