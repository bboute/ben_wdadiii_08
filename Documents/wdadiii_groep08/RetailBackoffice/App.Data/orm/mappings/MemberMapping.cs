﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using App.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Data.orm.mappings
{
    internal class MemberMapping : EntityTypeConfiguration<Member>
    {
        public MemberMapping()
            : base()
        {
            this.ToTable("members", "retail");

            this.Property(t => t.Password).HasColumnName("member_password").IsRequired().IsFixedLength().HasMaxLength(60);
            this.Property(t => t.PasswordSalt).HasColumnName("member_salt").IsRequired().IsFixedLength().HasMaxLength(30);
            this.Property(t => t.ConfirmationToken).HasColumnName("member_token").IsOptional().HasMaxLength(128);
            this.Property(t => t.ConfirmationDate).HasColumnName("member_confirmed").IsOptional();
        }
    }
}
