﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using App.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Data.orm.mappings
{
    internal class EntityCommentMapping : EntityTypeConfiguration<EntityComment>
    {
        public EntityCommentMapping()
            : base()
        {
            this.ToTable("entitycomments", "retail");

            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("comment_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(t => t.Body).HasColumnName("comment_body").IsRequired();
            this.Property(t => t.CreatedDate).HasColumnName("comment_created").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            this.Property(t => t.ModifiedDate).HasColumnName("comment_modified").IsOptional();
            this.Property(t => t.DeletedDate).HasColumnName("comment_deleted").IsOptional();
            this.Property(t => t.LockedDate).HasColumnName("comment_locked").IsOptional();

            this.Property(t => t.ParentId).HasColumnName("comment_parentid").IsOptional();
            this.Property(t => t.UserId).HasColumnName("user_id").IsRequired();
            this.Property(t => t.EntityId).HasColumnName("entity_id").IsRequired();

            //FOREIGN KEYS
            this.HasRequired(t => t.User).WithMany(t => t.Comments).HasForeignKey(t => t.UserId);
            this.HasRequired(t => t.Entity).WithMany(t => t.Comments).HasForeignKey(t => t.EntityId);
            this.HasRequired(t => t.ParentComment).WithMany(t => t.ChildComments).HasForeignKey(t => t.ParentId);
        }
    }
}
