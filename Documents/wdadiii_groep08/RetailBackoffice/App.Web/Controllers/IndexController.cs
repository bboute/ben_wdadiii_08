﻿using App.Data.orm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using System.Web.Security;

namespace App.Web.Controllers
{
    public class IndexController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }

    }
}
