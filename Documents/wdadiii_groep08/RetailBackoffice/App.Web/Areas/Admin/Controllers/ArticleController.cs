﻿using App.Data.orm;
using App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.Web.Areas.Admin.Controllers
{
    public class ArticleController : AdminController
    {        
        public ActionResult Index()
        {
            var articles = UnitOfWork.ArticleRepository.Get();
            return View(articles);
        }
        public ActionResult Create()
        {
            var model = new App.Web.Areas.Admin.Models.ArticleViewModel
            {
                Article = new Article(),
                Categories = new MultiSelectList(this.UnitOfWork.CategoryRepository.Get(), "Id", "Name")
            };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(App.Web.Areas.Admin.Models.ArticleViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception();

                var article = viewModel.Article;
                var ids = viewModel.SelectedCategoryIds;
                if (ids != null && ids.Length > 0)
                {
                    var categories = new List<Category>();
                    foreach (var id in ids)
                    {
                        var category = this.UnitOfWork.CategoryRepository.GetByID(id);
                        categories.Add(category);
                    }
                    article.Categories = categories;
                }
                
                this.UnitOfWork.ArticleRepository.Insert(article);
                this.UnitOfWork.Save();

                return RedirectToAction("Index", "Article", new { area = "Admin" });
            }
            catch
            {
                var model = new App.Web.Areas.Admin.Models.ArticleViewModel
                {
                    Article = viewModel.Article,
                    Categories = new MultiSelectList(this.UnitOfWork.CategoryRepository.Get(), "Id", "Name")
                };
                return View(model);
            }
        }
        public ActionResult Edit(Int64 id)
        {
            try
            {
                var article = this.UnitOfWork.ArticleRepository.GetByID(id);
                if (article == null)
                    throw new Exception();

                int[] ids = null;
                if (article.Categories != null && article.Categories.Count > 0)
                {
                    ids = new int[article.Categories.Count];
                    int i = 0;
                    foreach (var category in article.Categories)
                    {
                        ids[i] = category.Id;
                        i++;
                    }
                }
                var model = new App.Web.Areas.Admin.Models.ArticleViewModel
                {
                    Article = this.UnitOfWork.ArticleRepository.GetByID(id),
                    SelectedCategoryIds = ids,
                    Categories = new MultiSelectList(this.UnitOfWork.CategoryRepository.Get(), "Id", "Name")
                };
                return View(model);
            }
            catch
            {
                return RedirectToAction("Index", "Article", new { area = "Admin" });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(App.Web.Areas.Admin.Models.ArticleViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception();

                var originalArticle = this.UnitOfWork.ArticleRepository.GetByID(viewModel.Article.Id);

                if (originalArticle == null)
                    throw new Exception();

                originalArticle.Title = viewModel.Article.Title;
                originalArticle.Description = viewModel.Article.Description;
                originalArticle.Body = viewModel.Article.Body;
                originalArticle.Price = viewModel.Article.Price;
                originalArticle.ModifiedDate = DateTime.Now;

                var ids = viewModel.SelectedCategoryIds;
                if (ids != null && ids.Length > 0)
                {
                    if (originalArticle.Categories != null)
                        originalArticle.Categories.Clear();

                    var categories = new List<Category>();
                    foreach (var id in ids)
                    {
                        var category = this.UnitOfWork.CategoryRepository.GetByID(id);
                        categories.Add(category);
                    }
                    originalArticle.Categories = categories;
                }

                this.UnitOfWork.ArticleRepository.Update(originalArticle);
                this.UnitOfWork.Save();

                return RedirectToAction("Index", "Article", new { area = "Admin" });
            }
            catch
            {
                var model = new App.Web.Areas.Admin.Models.ArticleViewModel
                {
                    Article = viewModel.Article,
                    SelectedCategoryIds = viewModel.SelectedCategoryIds,
                    Categories = new MultiSelectList(this.UnitOfWork.CategoryRepository.Get(), "Id", "Name")
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(Int64 id)
        {
            try
            {
                var model = this.UnitOfWork.ArticleRepository.GetByID(id);
                if(model == null)
                    throw new Exception();

                this.UnitOfWork.ArticleRepository.Delete(model);
                this.UnitOfWork.Save();

                var message = String.Format("Deleted the article with id '{0}' from the database!", model.Id);
                return Json(new {state=1, id=model.Id, message=message});
            }
            catch
            {
                var message = String.Format("Could not delete the article with id '{0}' from the database!", id);
                return Json(new { state = 0, id = id, message = message });
            }
        }
    }
}
  