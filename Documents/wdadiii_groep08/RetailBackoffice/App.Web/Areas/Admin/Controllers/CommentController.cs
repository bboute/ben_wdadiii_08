﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;

namespace App.Web.Areas.Admin.Controllers
{
    public class CommentController : AdminController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Delete(Int64 id)
        {
            try
            {
                var model = this.UnitOfWork.EntityCommentRepository.GetByID(id);
                if (model == null)
                    throw new Exception();

                this.UnitOfWork.EntityCommentRepository.Delete(id);
                this.UnitOfWork.Save(); 
                
                var message = String.Format("Deleted the comment with id '{0}' from the database!", model.Id);
                return Json(new { state = 1, id = model.Id, message = message });
            }
            catch
            {
                var message = String.Format("Could not delete the comment with id '{0}' from the database!", id);
                return Json(new { state = 0, id = id, message = message });
            }
        }

        [HttpPost]
        public ActionResult VirtualDelete(Int64 id)
        {
            try
            {
                var model = this.UnitOfWork.EntityCommentRepository.GetByID(id);
                if (model == null)
                    throw new Exception();

                model.DeletedDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                this.UnitOfWork.EntityCommentRepository.Update(model);
                this.UnitOfWork.Save();

                var message = String.Format("Virtual deleted the comment with id '{0}' from the database!", model.Id);
                return Json(new { state = 1, id = model.Id, message = message });
            }
            catch
            {
                var message = String.Format("Could not virtual delete the comment with id '{0}' from the database!", id);
                return Json(new { state = 0, id = id, message = message });
            }
        }

        [HttpPost]
        public ActionResult VirtualUnDelete(Int64 id)
        {
            try
            {
                var model = this.UnitOfWork.EntityCommentRepository.GetByID(id);
                if (model == null)
                    throw new Exception();

                model.DeletedDate = null;
                model.ModifiedDate = DateTime.Now;

                this.UnitOfWork.EntityCommentRepository.Update(model);
                this.UnitOfWork.Save();

                var message = String.Format("Virtual undeleted the comment with id '{0}' from the database!", model.Id);
                return Json(new { state = 1, id = model.Id, message = message });
            }
            catch
            {
                var message = String.Format("Could not virtual undelete the comment with id '{0}' from the database!", id);
                return Json(new { state = 0, id = id, message = message });
            }
        }

        [HttpPost]
        public ActionResult Lock(Int64 id)
        {
            try
            {
                var model = this.UnitOfWork.EntityCommentRepository.GetByID(id);
                if (model == null)
                    throw new Exception();

                model.LockedDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                this.UnitOfWork.EntityCommentRepository.Update(model);
                this.UnitOfWork.Save();

                var message = String.Format("Locked the comment with id '{0}' from the database!", model.Id);
                return Json(new { state = 1, id = model.Id, message = message });
            }
            catch
            {
                var message = String.Format("Could not lock the comment with id '{0}' from the database!", id);
                return Json(new { state = 0, id = id, message = message });
            }
        }

        [HttpPost]
        public ActionResult UnLock(Int64 id)
        {
            try
            {
                var model = this.UnitOfWork.EntityCommentRepository.GetByID(id);
                if (model == null)
                    throw new Exception();

                model.LockedDate = null;
                model.ModifiedDate = DateTime.Now;

                this.UnitOfWork.EntityCommentRepository.Update(model);
                this.UnitOfWork.Save();

                var message = String.Format("UnLocked the comment with id '{0}' from the database!", model.Id);
                return Json(new { state = 1, id = model.Id, message = message });
            }
            catch
            {
                var message = String.Format("Could not unlock the comment with id '{0}' from the database!", id);
                return Json(new { state = 0, id = id, message = message });
            }
        }

        public ActionResult PVCommentsList(string sort, string direction, string filter, int? page)
        {
            var pageNumber = page ?? 1;
            var tempSort = String.IsNullOrEmpty(sort) ? "" : sort;
            var tempDirection = String.IsNullOrEmpty(direction) ? "asc" : direction;
            var tempFilter = String.IsNullOrEmpty(filter) ? "" : filter;

            ViewBag.Sort = tempSort;
            ViewBag.Direction = tempDirection;
            ViewBag.Filter = tempFilter;

            var model = this.UnitOfWork.EntityCommentRepository.Get(
                filter:
                c => c.User.UserName.Contains(tempFilter)
                );

            if (!String.IsNullOrEmpty(sort))
            {
                switch (sort)
                {
                    case "sender":
                        if (direction == "asc")
                            model.OrderBy(c => c.User.UserName).ThenByDescending(c => c.CreatedDate).ThenByDescending(c => c.ModifiedDate);
                        else
                            model.OrderByDescending(c => c.User.UserName).ThenByDescending(c => c.CreatedDate).ThenByDescending(c => c.ModifiedDate);
                        break;
                }
            }
            else
            {
                model.OrderByDescending(c => c.CreatedDate).ThenByDescending(c => c.ModifiedDate);
            }

            var pagedModel = model.ToPagedList(pageNumber, 10);

            return PartialView("_CommentsListPartial", pagedModel);
        }

    }
}
