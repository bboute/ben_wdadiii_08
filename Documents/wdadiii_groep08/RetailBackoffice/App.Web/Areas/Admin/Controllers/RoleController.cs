﻿using App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.Web.Areas.Admin.Controllers
{
    public class RoleController : AdminController
    {
        public ActionResult Index()
        {
            var model = this.UnitOfWork.RoleRepository.Get();
            return View(model);
        }
        public ActionResult Create()
        {
            var model = new Role();
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Role role)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(role);
                this.UnitOfWork.RoleRepository.Insert(role);
                this.UnitOfWork.Save();

                return RedirectToAction("Index", "Role", new { area = "Admin" });
            }
            catch
            {
                return View(role);
            }
        }
        public ActionResult Edit(Int16 id)
        {
            try
            {
                var model = this.UnitOfWork.RoleRepository.GetByID(id);
                return View(model);
            }
            catch
            {
                return RedirectToAction("Index", "Role", new { area = "Admin" });
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Role role)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(role);

                var originalRole = this.UnitOfWork.RoleRepository.GetByID(role.Id);

                if (originalRole == null)
                    throw new Exception();

                originalRole.Name = role.Name;
                originalRole.Description = role.Description;
                originalRole.ModifiedDate = DateTime.Now;

                this.UnitOfWork.RoleRepository.Update(originalRole);
                this.UnitOfWork.Save();

                return RedirectToAction("Index", "Role", new { area = "Admin" });
            }
            catch
            {
                return View(role);
            }
        }
        [HttpPost]
        public ActionResult Delete(Int32 id)
        {
            try
            {
                var model = this.UnitOfWork.RoleRepository.GetByID(id);
                if (model == null)
                    throw new Exception();

                this.UnitOfWork.RoleRepository.Delete(model);
                this.UnitOfWork.Save();

                var message = String.Format("Deleted the role with id '{0}' from the database!", model.Id);
                return Json(new { state = 1, id = model.Id, message = message });
            }
            catch
            {
                var message = String.Format("Could not delete the role with id '{0}' from the database!", id);
                return Json(new { state = 0, id = id, message = message });
            }
        }
    }
}
