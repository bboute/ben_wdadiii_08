﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Models;

namespace App.Web.Areas.Admin.Controllers
{
    public class CategoryController : AdminController
    {
        public ActionResult Index()
        {
            var model = this.UnitOfWork.CategoryRepository.Get();
            return View(model);
        }

        public ActionResult Create()
        {
            var model = new App.Web.Areas.Admin.Models.CategoryViewModel
            {
                Category = new Category(),
                Categories = this.UnitOfWork.CategoryRepository.Get()
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(App.Web.Areas.Admin.Models.CategoryViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception();

                this.UnitOfWork.CategoryRepository.Insert(viewModel.Category);
                this.UnitOfWork.Save();

                return RedirectToAction("Index", "Category", new { area = "Admin" });
            }
            catch
            {
                var model = new App.Web.Areas.Admin.Models.CategoryViewModel
                {
                    Category = viewModel.Category,
                    Categories = this.UnitOfWork.CategoryRepository.Get()
                };
                return View(model);
            }            
        }

        [HttpPost]
        public ActionResult Delete(Int16 id)
        {
            try
            {
                var model = this.UnitOfWork.CategoryRepository.GetByID(id);
                if (model == null)
                    throw new Exception();

                this.UnitOfWork.CategoryRepository.Delete(model);
                this.UnitOfWork.Save();

                var message = String.Format("Deleted the category with id '{0}' and name '{1}' from the database!", model.Id,model.Name);
                return Json(new { state = 1, id = model.Id, message = message });
            }
            catch
            {
                var message = String.Format("Could not delete the category with id '{0}' from the database!", id);
                return Json(new { state = 0, id = id, message = message });
            }
        }
    }
}
