﻿using App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.Web.Areas.Admin.Controllers
{
    public class HomeController : AdminController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PVAmountPerEntity()
        {
            var model = new App.Web.Areas.Admin.Models.AmountPerEntityViewModel
            {
                AmountArticles = this.UnitOfWork.ArticleRepository.Get().Count<Article>(),
                AmountComments = this.UnitOfWork.EntityCommentRepository.Get().Count<EntityComment>(),
                AmountCategories = this.UnitOfWork.CategoryRepository.Get().Count<Category>(),
                AmountUsers = this.UnitOfWork.UserRepository.Get().Count<User>(),
                AmountRoles = this.UnitOfWork.RoleRepository.Get().Count<Role>()
            };

            return PartialView("_AmountPerEntityPartial", model);
        }
    }
}
