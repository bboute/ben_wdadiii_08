﻿using App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.Web.Areas.Admin.Controllers
{
    public class UserController : AdminController
    {
        public ActionResult Index()
        {
            var model = this.UnitOfWork.UserRepository.Get();
            return View(model);
        }

        public ActionResult Edit(Int32 id)
        {
            try
            {
                var user = this.UnitOfWork.UserRepository.GetByID(id);
                if (user == null)
                    throw new Exception();

                int[] ids = null;
                if (user.Roles != null && user.Roles.Count > 0)
                {
                    ids = new int[user.Roles.Count];
                    int i = 0;
                    foreach (var role in user.Roles)
                    {
                        ids[i] = role.Id;
                        i++;
                    }
                }
                var model = new App.Web.Areas.Admin.Models.UserEditViewModel
                {
                    User = this.UnitOfWork.UserRepository.GetByID(id),
                    SelectedRoleIds = ids,
                    Roles = new MultiSelectList(this.UnitOfWork.RoleRepository.Get(), "Id", "Name")
                };
                return View(model);
            }
            catch
            {
                return RedirectToAction("Index", "User", new { area = "Admin" });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(App.Web.Areas.Admin.Models.UserEditViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception();

                var originalUser = this.UnitOfWork.UserRepository.GetByID(viewModel.User.Id);

                if (originalUser == null)
                    throw new Exception();

                originalUser.UserName = viewModel.User.UserName;
                originalUser.Email = viewModel.User.Email;

                originalUser.Person.FirstName = viewModel.User.Person.FirstName;
                originalUser.Person.SurName = viewModel.User.Person.SurName;
                
                originalUser.ModifiedDate = DateTime.Now;

                var ids = viewModel.SelectedRoleIds;
                if (ids != null && ids.Length > 0)
                {
                    if (originalUser.Roles != null)
                        originalUser.Roles.Clear();

                    var roles = new List<Role>();
                    foreach (var id in ids)
                    {
                        var role = this.UnitOfWork.RoleRepository.GetByID(id);
                        roles.Add(role);
                    }
                    originalUser.Roles = roles;
                }

                this.UnitOfWork.UserRepository.Update(originalUser);
                this.UnitOfWork.Save();

                return RedirectToAction("Index", "User", new { area = "Admin" });
            }
            catch
            {
                var model = new App.Web.Areas.Admin.Models.UserEditViewModel
                {
                    User = viewModel.User,
                    SelectedRoleIds = viewModel.SelectedRoleIds,
                    Roles = new MultiSelectList(this.UnitOfWork.RoleRepository.Get(), "Id", "Name")
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(Int64 id)
        {
            try
            {
                var model = this.UnitOfWork.UserRepository.GetByID(id);
                if (model == null)
                    throw new Exception();

                this.UnitOfWork.UserRepository.Delete(model);
                this.UnitOfWork.Save();

                var message = String.Format("Deleted the user with id '{0}' from the database!", model.Id);
                return Json(new { state = 1, id = model.Id, message = message });
            }
            catch
            {
                var message = String.Format("Could not delete the user with id '{0}' from the database!", id);
                return Json(new { state = 0, id = id, message = message });
            }
        }


    }
}
