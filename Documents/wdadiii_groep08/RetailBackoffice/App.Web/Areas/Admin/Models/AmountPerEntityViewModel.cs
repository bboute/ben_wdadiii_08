﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Web.Areas.Admin.Models
{
    public class AmountPerEntityViewModel
    {
        public int AmountArticles { get; set; }
        public int AmountComments { get; set; }
        public int AmountCategories { get; set; }
        public int AmountUsers { get; set; }
        public int AmountRoles { get; set; }
    }
}