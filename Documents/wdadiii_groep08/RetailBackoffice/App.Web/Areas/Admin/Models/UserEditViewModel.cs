﻿using App.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.Web.Areas.Admin.Models
{
    public class UserEditViewModel 
    {
        public User User { get; set; }
        public int[] SelectedRoleIds { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }
    }
}
