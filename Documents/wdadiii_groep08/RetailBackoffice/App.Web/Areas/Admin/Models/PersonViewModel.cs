﻿using App.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace App.Web.Areas.Admin.Models
{
    public class PersonViewModel
    {
        [Required(ErrorMessage = "Firstname is required")]
        [StringLength(45)]
        [DisplayName("Firstname")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Surname is required")]
        [StringLength(255)]
        [DisplayName("Surname")]
        public string SurName { get; set; }

        public PersonProfile PersonProfile;
    }
}