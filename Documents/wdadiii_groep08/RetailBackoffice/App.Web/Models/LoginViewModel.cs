﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.Web.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Username is required")]
        [StringLength(45)]
        [DisplayName("UserName")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [StringLength(15)]
        [DisplayName("Password")]
        [AllowHtml]
        public string Password { get; set; }
        [DisplayName("Remember me?")]
        public Boolean RememberMe { get; set; }
    }
}