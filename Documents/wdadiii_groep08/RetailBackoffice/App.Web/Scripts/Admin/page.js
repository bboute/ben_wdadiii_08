﻿(function () {
    $('#sidebar-left a').on('click', function (e) {
        if ($(this).attr('href') == '#') {
            e.preventDefault();
            if (!$(this).hasClass("collapsed")) {
                var lastActive = $(this).parent().siblings();
                $(lastActive).each(function (e, obj) {
                    $(obj).find('a').removeClass("collapsed");
                    $(obj).find('a').next('ul').collapse('hide');
                });
                $(this).addClass("collapsed");
                $(this).next('ul').collapse('show');
            } else {
                $(this).removeClass("collapsed");
                $(this).next('ul').collapse('hide');
            }
            return false;
        }
    });

    setMenuByRoute();

    $(window).resize(function (e) {
        setMainHeight();
    });
    setMainHeight();

    setupConfirmDialog();//Setup Global Delete Dialog
})();

function setMenuByRoute() {
    var route = (window.location.href.toString().split(window.location.host)[1]);
    if (route.toLowerCase() == "/admin/home/index") {
        route = "/admin/";
    }
    var activeLink = ($('#sidebar-left a[href="' + route + '"]'));
    $(activeLink).addClass("active");
    $(activeLink).next('ul').collapse('show');
    var parentActiveLink = $(activeLink).parent().parent().siblings('a')
    $(parentActiveLink).addClass("active");
    $(parentActiveLink).next('ul').collapse('show');
}

function setMainHeight() {
    var mainHeight = $('#main').height();
    var mainY = $('#main').offset().top;
    var winHeight = $(window).height();
    if (mainY + mainHeight < winHeight) {
        $('#main').css('height', winHeight-mainY + 'px');
    }
}

var confirmLinkCategory;
var confirmMessage;
var confirmLinkHref;
var confirmAction;
function setupConfirmDialog() {
    $('.confirm-link').click(function (ev) {
        ev.preventDefault();

        confirmLinkCategory = $(this).data('cat');
        confirmAction = $(this).data('action');

        var id = $(this).data('id');
        var row = $('tr[data-id="' + confirmLinkCategory + '-' + id + '"]');

        switch (confirmAction) {
            case "delete":
                confirmMessage = "Are you sure you want to delete the selected " + confirmLinkCategory + ": " + $(row).data('title') + "?";
                break;
            case "lock":
                confirmMessage = "Are you sure you want to lock the selected " + confirmLinkCategory + ": " + $(row).data('title') + "?";
                break;
            case "unlock":
                confirmMessage = "Are you sure you want to unlock the selected " + confirmLinkCategory + ": " + $(row).data('title') + "?";
                break;
            case "virtualdelete":
                confirmMessage = "Are you sure you want to virtual delete the selected " + confirmLinkCategory + ": " + $(row).data('title') + "?";
                break;
            case "virtualundelete":
                confirmMessage = "Are you sure you want to virtual undelete the selected " + confirmLinkCategory + ": " + $(row).data('title') + "?";
                break;
        }

        confirmLinkHref = $(this)[0].href;
        $('#confirm-dialog .modal-body').html(confirmMessage);
        $('#confirm-dialog').modal('show');
        return false;
    });

    $('#confirm-dialog .btn-confirm').click(function (ev) {
        $.post(confirmLinkHref, function (data) {
            if (data.state == 1) {
                $('#alert-message').html(data.message);
                $('#alert-message').removeClass('alert-info');
                $('#alert-message').removeClass('alert-danger');
                $('#alert-message').addClass('alert-success');
                var row = $('tr[data-id="' + confirmLinkCategory + '-' + data.id + '"]');
                var updateTarget = $(row).data('update-target');
                var updateHref = $(row).data('update-href');

                switch (confirmAction) {
                    case "delete":
                        $(row).effect('highlight', {}, 750, function () {
                            $(this).fadeOut('slow', function () {
                                $(this).remove();

                                $.get(updateHref, function (data) {
                                    $(updateTarget).html(data);

                                    setupConfirmDialog();
                                });
                            });
                        });
                        break;
                    case "lock": case "unlock": case "virtualdelete": case "virtualundelete":
                        $(row).effect('highlight', {}, 750, function () {
                            $.get(updateHref, function (data) {
                                $(updateTarget).html(data);

                                setupConfirmDialog();
                            });
                        });
                }

                

            } else {
                $('#alert-message').html(data.message);
                $('#alert-message').removeClass('alert-info');
                $('#alert-message').removeClass('alert-success');
                $('#alert-message').addClass('alert-danger');
            }
        });
        $('#confirm-dialog').modal('hide');
    });
}