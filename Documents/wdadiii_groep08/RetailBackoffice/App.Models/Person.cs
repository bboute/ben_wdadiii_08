﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models
{
    public class Person
    {
        public Int32 Id { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string Profile {
            get
            {
                if (PersonProfile != null)
                    return JsonConvert.SerializeObject(PersonProfile);
                else
                    return null;
            }
            set
            {
                if(value != null)
                    PersonProfile = JsonConvert.DeserializeObject<PersonProfile>(value);
            }
        }
        public DateTime CreateDate { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public Nullable<DateTime> DeletedDate { get; set; }

        [NotMapped]
        public PersonProfile PersonProfile { get; set;}//JSON Person's Profile
    }
}
