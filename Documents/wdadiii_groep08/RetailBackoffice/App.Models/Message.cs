﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models
{
    public class Message
    {
        public string Id { get; set; }
        public string Body { get; set; }

        public DateTime CreateDate { get; set; }       
        public Nullable<DateTime> DeletedDate { get; set; }

        public Int32 UserId { get; set; }
        public Int32 ReceiverId { get; set; }

        public virtual User User { get; set; }
        public virtual User Receiver { get; set; }
    }
}
