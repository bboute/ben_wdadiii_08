﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models
{
    public abstract class Entity
    {
        public Int64 Id { get; set; }
        [Required(ErrorMessage="Title is required")]
        [StringLength(255)]
        [DisplayName("Title")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Description is required")]
        [DisplayName("Description")]
        public string Description { get; set; }
        [Required]
        public DateTime CreateDate { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public Nullable<DateTime> DeletedDate { get; set; }
        [Required]
        public Double Price { get; set; }

        [Required]
        [DisplayName("Id from User")]
        public Int32 UserId { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<EntityComment> Comments { get; set; }
        public virtual ICollection<User> Likes { get; set; }
    }
}
