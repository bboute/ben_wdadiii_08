﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models
{
    public class Member : User
    {
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string ConfirmationToken { get; set; }
        public Nullable<DateTime> ConfirmationDate { get; set; }
    }
}
