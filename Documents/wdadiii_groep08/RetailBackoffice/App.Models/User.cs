﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models
{
    public class User
    {
        public Int32 Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime CreatedDate { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public Nullable<DateTime> DeletedDate { get; set; }
        public Nullable<DateTime> LockedDate { get; set; }
        public Nullable<DateTime> LastLoggedInDate { get; set; }
        public string AvatarURL { get; set; }
        public Nullable<Boolean> AvatarIsExternal { get; set; }

        public Int32 PersonId { get; set; }

        public virtual Person Person { get; set; }
        public virtual ICollection<Entity> Entities { get; set; }
        public virtual ICollection<EntityComment> Comments { get; set; }
        public virtual ICollection<Entity> Likes { get; set; }

        public virtual ICollection<Message> SentMessages { get; set; }
        public virtual ICollection<Message> ReceivedMessages { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}
