﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models
{
    public class PersonProfile
    {
        [DataType(DataType.DateTime)]
        [DisplayName("Date of birth")]
        public Nullable<DateTime> DayOfBirth { get; set; }

        [StringLength(15)]
        [DisplayName("Gender")]
        public string Gender { get; set; }
    }
}
