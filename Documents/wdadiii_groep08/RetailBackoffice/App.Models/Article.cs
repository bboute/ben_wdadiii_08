﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace App.Models
{
    public class Article : Entity
    {
        [Required(ErrorMessage = "Body is required")]
        [DisplayName("Body")]
        [AllowHtml]
        public string Body { get; set; }
    }
}
