﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using App.Models;
using App.Data.orm;

namespace App.UnitTest
{
    [TestClass]
    public class Retail
    {
        [TestMethod]
        public void InsertMember()
        {
            UnitOfWork unitOfWork = new UnitOfWork();

            Member member = new Member();
            member.UserName = "Thomas";
            member.Email = "thomas@gmail.com";
            member.PasswordSalt = BCrypt.Net.BCrypt.GenerateSalt(12);
            member.Password = BCrypt.Net.BCrypt.HashPassword("admin", member.PasswordSalt);

            Person person = new Person();
            person.FirstName = "thomas";
            person.SurName = ".";

            member.Person = person;

            unitOfWork.MemberRepository.Insert(member);
            unitOfWork.Save();
        }

        [TestMethod]
        public void InsertArticle()
        {
            UnitOfWork unitOfWork = new UnitOfWork();

            Article article = new Article();
            article.Title = "BMW K75 - Nieuw";
            article.Description = "Nieuwe motor met 100 km op de teller";
            article.Body = @"
                        <p>Hallo, ik doe mijn nieuwe bmw K75 weg, omdat dit de slechste aankoop in mijn leven is geweest. Met de motor zelf niets mis, maar financieel kon ik die aankoop niet aan. Ik doe hem weg voor 1000 euro minder dan de aankoopprijs, alle papieren zijn aanwezig.</p>
                            ";

            article.Price = 7500.00;
            article.User = unitOfWork.UserRepository.GetByID(1);
            unitOfWork.ArticleRepository.Insert(article);
            unitOfWork.Save();
        }
        
        [TestMethod]
        public void InsertCategories()
        {
            UnitOfWork unitOfWork = new UnitOfWork();

            Category category1 = new Category();
            category1.Name = "Voertuigen";
            category1.Description = "alle voertuigen";
            unitOfWork.CategoryRepository.Insert(category1);

            Category category2 = new Category();
            category2.Name = "Moto's";
            category2.Description = "alle moto's";
            category2.ParentCategory = unitOfWork.CategoryRepository.GetByID(1);
            unitOfWork.CategoryRepository.Insert(category2);
            
            unitOfWork.Save();
        }

        [TestMethod]
        public void AddCategoriesToArticle()
        {
            UnitOfWork unitOfWork = new UnitOfWork();

            var article = unitOfWork.ArticleRepository.GetByID(1);
            var cat1 = unitOfWork.CategoryRepository.GetByID(1);
            var cat2 = unitOfWork.CategoryRepository.GetByID(2);

            article.Categories.Add(cat1);
            article.Categories.Add(cat2);

            unitOfWork.ArticleRepository.Update(article);
            unitOfWork.Save();
        }

        [TestMethod]
        public void InsertComments()
        {
            UnitOfWork unitOfWork = new UnitOfWork();

            var user = unitOfWork.UserRepository.GetByID(1);//User

            var entity = unitOfWork.ArticleRepository.GetByID(1);//Article

            EntityComment comment1 = new EntityComment();
            comment1.Body = @"2500";
            comment1.User = user;
            comment1.Entity = entity;
            unitOfWork.EntityCommentRepository.Insert(comment1);

            unitOfWork.Save();
        }

        [TestMethod]
        public void InsertMessage()
        {
            UnitOfWork unitOfWork = new UnitOfWork();

            var user = unitOfWork.UserRepository.GetByID(1);//User
            var friend = unitOfWork.UserRepository.GetByID(3);//User

            Message message = new Message();
            message.Id = Guid.NewGuid().ToString();
            message.Body = @"Testbericht.
                            ";
            message.User = user;
            message.Receiver = friend;

            unitOfWork.MessageRepository.Insert(message);
            unitOfWork.Save();
        }

        [TestMethod]
        public void InsertRoles()
        {
            UnitOfWork unitOfWork = new UnitOfWork();

            Role role1 = new Role();
            role1.Name = "Administrator";
            role1.Description = "Administrator";
            unitOfWork.RoleRepository.Insert(role1);

            Role role2 = new Role();
            role2.Name = "User";
            role2.Description = "User";
            unitOfWork.RoleRepository.Insert(role2);

            unitOfWork.Save();
        }

        [TestMethod]
        public void AddRolesToUser()
        {
            UnitOfWork unitOfWork = new UnitOfWork();

            var user = unitOfWork.UserRepository.GetByID(1);//User
            var roles = unitOfWork.RoleRepository.Get();

            foreach (var role in roles)
            {
                user.Roles.Add(role);
            }
            unitOfWork.UserRepository.Update(user);
            unitOfWork.Save();
        }

        [TestMethod]
        public void GetArticles()
        {
            UnitOfWork unitOfWork = new UnitOfWork();

            var articles = unitOfWork.ArticleRepository.Get();
        }

    }
}
