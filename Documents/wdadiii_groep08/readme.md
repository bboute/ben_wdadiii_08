## WDADIII-RETAIL
## DE Tweedehandswebsite voor u en uw gezin!

## Groep 08
- Ben Boute
- Thomas Seyssens

## Backoffice

1. Download de zip van bitbucket
2. Kopieer naar je documenten
3. Open  het visual studio project
4. Run het project (play button)



## Frontoffice

1. In de zip file vind je de map frontoffice
2. kopieer deze map naar je  htdocs van de lokale server
3. Cd naar deze map, en gebruik de bash command $ composer install
4. surf in je webbrowser naar het volgende adres: localhost:XXXX/htdocs_map_naam/web/app_dev.php/index



